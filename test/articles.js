//During the test the env variable is set to test
process.env.NODE_ENV = "test";

//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let expect = chai.expect;

chai.use(chaiHttp);

//Test to get all article
describe("/GET article", () => {
  it("it should GET all the article", done => {
    chai
      .request("http://localhost:5000")
      .get("/api/articles/page/1")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });
});

//Test to get an article
describe("/GET an article (by id)", () => {
  it("it should GET a specific article", done => {
    chai
      .request("http://localhost:5000")
      .get("/api/articles/-LWotEmUuJl2ps0-WvsP")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });
});

//Test to POST an Article without nickname
describe("/POST an article", () => {
  it("it should not POST an article without nickname", done => {
    let article = {
      title: "Brewing Manual",
      content: "How to Brew",
      creation_date: new Date().getTime()
    };
    chai
      .request("http://localhost:5000")
      .post("/api/articles")
      .send(article)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res)
          .to.have.property("body")
          .and.has.property("nickname");
        done();
      });
  });
});

//Test to POST an Article without title
describe("/POST an article", () => {
  it("it should not POST an article without title", done => {
    let article = {
      nickname: "John Doe",
      content: "How to Brew",
      creation_date: new Date().getTime()
    };
    chai
      .request("http://localhost:5000")
      .post("/api/articles")
      .send(article)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res)
          .to.have.property("body")
          .and.has.property("title");
        done();
      });
  });
});

//Test to POST an Article without content
describe("/POST an article", () => {
  it("it should not POST an article without content", done => {
    let article = {
      nickname: "John Doe",
      title: "Brewing Manual",
      creation_date: new Date().getTime()
    };
    chai
      .request("http://localhost:5000")
      .post("/api/articles")
      .send(article)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res)
          .to.have.property("body")
          .and.has.property("content");
        done();
      });
  });
});

//Test to POST an Article without creation date
describe("/POST an article", () => {
  it("it should not POST an article without creation date", done => {
    let article = {
      nickname: "John Doe",
      title: "Brewing Manual",
      content: "How to Brew"
    };
    chai
      .request("http://localhost:5000")
      .post("/api/articles")
      .send(article)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res)
          .to.have.property("body")
          .and.has.property("creation_date");
        done();
      });
  });
});
//});
