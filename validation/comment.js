const isEmpty = require("./is-empty");
module.exports = function validateComment(data) {
  let errors = {};

  if (isEmpty(data.nickname)) {
    errors.nickname = "Nickname is required";
  }
  if (isEmpty(data.content)) {
    errors.content = "Content is required";
  }
  if (isEmpty(data.creation_date)) {
    errors.creation_date = "Creation date is required";
  }
  if (isEmpty(data.article_id)) {
    errors.article_id = "Article id is required";
  }

  return {
    errors: errors,
    isValid: isEmpty(errors)
  };
};
